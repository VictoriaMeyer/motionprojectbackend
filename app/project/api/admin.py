from django.contrib import admin

from project.api.models import Post, PostLike, UserProfile

admin.site.register(Post)
admin.site.register(PostLike)
admin.site.register(UserProfile)
