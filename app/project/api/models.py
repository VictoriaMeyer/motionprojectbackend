from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from random import choice
from string import ascii_uppercase
from django.db.models.signals import post_save
from django.dispatch import receiver

User = get_user_model()


class Post(models.Model):
    class Meta:
        ordering = ['-created']
    user = models.ForeignKey(
        verbose_name='user',  # Representation name for django admin
        related_name='posts',  # Backward relation name for one user instance => all user posts
        to=settings.AUTH_USER_MODEL,  # The pointer to which table its related
        on_delete=models.CASCADE,  # What to do with the posts if the user gets deleted
    )
    created = models.DateTimeField(
        verbose_name='created',
        auto_now_add=True,
    )
    title = models.CharField(
        verbose_name='title',
        max_length=200,
    )
    content = models.TextField(
        verbose_name='content',
    )
    shared = models.ForeignKey(
        verbose_name='share',
        related_name='shares',
        to='self',
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )


class PostLike(models.Model):
    user = models.ForeignKey(
        verbose_name='user',
        related_name='likes',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    post = models.ForeignKey(
        verbose_name='post',
        related_name='likes',
        to='api.Post',
        on_delete=models.CASCADE,
    )


class UserProfile(models.Model):
    validation_code_length = 5
    user = models.OneToOneField(
        verbose_name='user',
        related_name='user_profile',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    followers = models.ManyToManyField(
        verbose_name='follower',
        related_name='followees',
        blank=True,
        to=settings.AUTH_USER_MODEL,
    )
    validation_code = models.CharField(
        verbose_name='validation_code',
        max_length=200,
        null=True,
        blank=True
    )

    def generate_validation_code(self):
        self.validation_code = ''.join(choice(ascii_uppercase) for i in range(self.validation_code_length))
        self.save()


@receiver(post_save, sender=User)
def create_user_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_user_profile(sender, instance, **kwargs):
    instance.user_profile.save()


class FriendRequest(models.Model):
    status = models.TextField(
        verbose_name='content',
    )
    created = models.DateTimeField(
        verbose_name='created',
        auto_now_add=True,
    )
    receiver = models.ForeignKey(
        verbose_name='user',
        related_name='received',
        to=User,
        on_delete=models.CASCADE,
    )
    requester = models.ForeignKey(
        verbose_name='user',
        related_name='requested',
        to=User,
        on_delete=models.CASCADE,
    )


class Registration(models.Model):
    email = models.TextField(
        verbose_name='email',
    )
    validation_code = models.TextField(
        verbose_name='validation_code',
    )
