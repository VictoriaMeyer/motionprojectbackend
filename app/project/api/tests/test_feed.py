from django.contrib.auth import get_user_model
from rest_framework import status
from project.api.models import Post
from project.api.tests.master_tests import MasterTestWrapper

User = get_user_model()


class FeedTests(MasterTestWrapper.MasterTests):
    endpoint = 'list-create-posts'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        for i in range(10):
            Post.objects.create(
                user=self.user,
                title=f'Post {i}',
                content='Die Welt ist schön!',
            )

    def test_method_not_allowed(self):
        url = self.get_url()  # pk als argument eingeben
        self.authorize()
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_feed_post_count(self):
        url = self.get_url()
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 10)

    def test_post_order_and_content(self):
        url = self.get_url()
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0].get('title'), 'Post 9')
        self.assertEqual(response.data[0].get('content'), 'Die Welt ist schön!')


class FeedByUserTests(MasterTestWrapper.MasterTests):
    endpoint = 'list-create-posts-by-user-id'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        for i in range(10):
            Post.objects.create(
                user=self.user,
                title=f'Post {i}',
                content='Die Welt ist schön!',
            )
            Post.objects.create(  # create posts for other user
                user=self.other_user,
                title=f'Post {i}',
                content='Die Welt ist schön!',
            )

    def get_kwargs(self):
        return {
            'pk': self.user.id
        }

    def test_method_not_allowed(self):
        url = self.get_url()  # Hier kann man die pk als argument eingeben
        self.authorize()
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_feed_post_count(self):
        url = self.get_url()
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 10)

    def test_post_order_and_content(self):
        url = self.get_url()
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0].get('title'), 'Post 9')
        self.assertEqual(response.data[0].get('content'), 'Die Welt ist schön!')


class FollowTests(MasterTestWrapper.MasterTests):
    endpoint = 'list-create-posts-by-user-id'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        for i in range(10):
            Post.objects.create(
                user=self.user,
                title=f'Post {i}',
                content='Die Welt ist schön!',
            )
            Post.objects.create(  # create posts for other user
                user=self.other_user,
                title=f'Post {i}',
                content='Die Welt ist schön!',
            )

    def get_kwargs(self):
        return {
            'pk': self.user.id
        }

    def test_method_not_allowed(self):
        url = self.get_url()  # Hier kann man die pk als argument eingeben
        self.authorize()
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_feed_post_count(self):
        url = self.get_url()
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 10)

    def test_post_order_and_content(self):
        url = self.get_url()
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0].get('title'), 'Post 9')
        self.assertEqual(response.data[0].get('content'), 'Die Welt ist schön!')


# Post followed Users
class PostsFollowedUser(MasterTestWrapper.MasterTests):
    endpoint = 'lists-posts-followed-users'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        for i in range(8):
            Post.objects.create(  # create posts for other user
                user=self.other_user,
                title=f'Post {i}',
                content='Posts followed user!',
            )

        self.other_user.user_profile.followers.add(self.user)

    def test_method_not_allowed(self):
        url = self.get_url()  # Hier kann man die pk als argument eingeben
        self.authorize()
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_feed_post_count(self):
        url = self.get_url()
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 8)

    def test_post_order_and_content(self):
        url = self.get_url()
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0].get('title'), 'Post 7')
        self.assertEqual(response.data[0].get('content'), 'Posts followed user!')


# Post search
class SearchPostTests(MasterTestWrapper.MasterTests):
    endpoint = 'post-search'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        for i in range(2):
            Post.objects.create(
                user=self.user,
                title=f'Post {i}',
                content='El mundo es bello!',
            )

    def test_method_not_allowed(self):
        url = self.get_url()  # Hier kann man die pk als argument eingeben
        self.authorize()
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_feed_post_count(self):
        url = self.get_url()
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_post_order_and_content(self):
        url = self.get_url() + '?search=mundo'
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0].get('title'), 'Post 1')
        self.assertEqual(response.data[0].get('content'), 'El mundo es bello!')
