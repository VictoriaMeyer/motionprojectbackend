from django.contrib.auth import get_user_model
from rest_framework import status
from project.api.tests.master_tests import MasterTestWrapper

User = get_user_model()


class RegistrationTests(MasterTestWrapper.BasicMasterTests):
    endpoint = 'registration'
    methods = ['POST']
    body = {
        'email': 'something@email.com',
    }

    def test_registration(self):
        url = self.get_url()
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json().get('detail'),
            'Registration validation email sent to something@email.com!'
        )
