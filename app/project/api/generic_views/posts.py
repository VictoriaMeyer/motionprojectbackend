from rest_framework.generics import RetrieveUpdateDestroyAPIView, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from project.api.models import Post, PostLike
from project.api.permissions import IsOwnerOrReadOnly
from project.api.serializers import PostSerializer


# Posts
class GetUpdateDeletePostView(RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [
        IsAuthenticated,
        IsOwnerOrReadOnly,
    ]


class CreateNewPostView(GenericAPIView):
    # POST: User can make a new post
    def post(self, request, **kwargs):
        serializer = PostSerializer(
            data=request.data,
            context={'request': request},
        )
        if serializer.is_valid(raise_exception=True):
            post = serializer.create(serializer.validated_data)
            return Response(PostSerializer(post).data)


class LikeDislikePostView(GenericAPIView):
    queryset = Post.objects.all()

    # POST: Like a post
    def post(self, request, pk, **kwargs):
        curr_post = self.get_object()
        PostLike.objects.get_or_create(user=request.user, post=curr_post)
        return Response({'detail': 'liked!'})

    # POST: Dislike a post
    def delete(self, request, pk, **kwargs):
        curr_post = self.get_object()
        PostLike.objects.get(user=request.user, post=curr_post).delete()
        return Response({'detail': 'Dislike!'})

    # GET: all likes
    def get(self, request, **kwargs):
        posts = Post.objects.filter(likes__user=request.user)
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data)


# POST: User can share a post
class SharePostView(GenericAPIView):
    queryset = Post.objects.all()

    def post(self, request, pk, **kwargs):
        sharedPost = self.get_object()
        Post.objects.create(shared=sharedPost, user=request.user)
        return Response({'detail': 'Post shared'})
