from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from project.api.models import User
from project.api.serializers import UserSerializer


class MeView(GenericAPIView):
    queryset = User.objects.all()

    # GET: Logged in user’s profile
    def get(self, request, **kwargs):
        me = request.user
        return Response(UserSerializer(me).data)

    # PUT: Update User Profile
    def put(self, request, **kwargs):
        me = User.objects.get(id=request.user.id)
        serializer = UserSerializer(me, data=request.data)
        if serializer.is_valid(raise_exception=True):
            me = serializer.save()
        return Response(UserSerializer(me).data)
