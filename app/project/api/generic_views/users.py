from django.db.models import Q
from rest_framework import generics, filters
from rest_framework.exceptions import NotFound
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from project.api.models import User, FriendRequest
from project.api.serializers import UserSerializer, PublicUserProfileSerializer, PendingRequestSerializer


class AllFollowersView(GenericAPIView):
    # GET: List of all the followers
    def get(self, request, **kwargs):
        curr_user = request.user
        followers = curr_user.user_profile.followers.all()
        serializer = UserSerializer(followers, many=True)
        return Response(serializer.data)


# POST: Follow a user
class FollowView(GenericAPIView):
    queryset = User.objects.all()

    def post(self, request, pk, **kwargs):

        user_to_follow = self.get_object()
        if request.user == user_to_follow:
            raise NotFound('Yourself')
        if request.user in user_to_follow.user_profile.followers.all():
            raise NotFound(f'you already follow this user!')
        user_to_follow.user_profile.followers.add(request.user)
        return Response({'detail': 'followed!'})

    # POST: Unfollow a user
    def delete(self, request, pk, **kwargs):
        user_to_unfollow = self.get_object()
        if request.user not in user_to_unfollow.user_profile.followers.all():
            raise NotFound(f'you are not follower of this user!')
        user_to_unfollow.user_profile.followers.remove(request.user)
        return Response({'detail': 'Unfollowed!'})


# GET: List of all the followees
class FolloweesView(GenericAPIView):
    def get(self, request, **kwargs):
        followees = [p.user for p in request.user.followees.all()]
        serializer = UserSerializer(followees, many=True)
        return Response(serializer.data)


# GET: all users
class AllUsersView(GenericAPIView):
    def get(self, request, **kwargs):
        users = User.objects.all()
        serializer = PublicUserProfileSerializer(users, many=True)
        return Response(serializer.data)


# GET User Search
class UserSearchView(generics.ListAPIView):
        queryset = User.objects.all()
        serializer_class = UserSerializer
        filter_backends = (filters.SearchFilter,)
        search_fields = ('id', 'username', 'first_name', 'last_name')


# GET: Public user profile
class PublicUsersView(GenericAPIView):
    queryset = User.objects.all()

    def get(self, request, pk, **kwargs):
        user_profile = self.get_object()
        return Response(PublicUserProfileSerializer(user_profile).data)


# POST: Friend Request
class FiendRequestView(GenericAPIView):
    queryset = User.objects.all()

    def post(self, request, pk, **kwargs):
        requested_user = self.get_object()
        curr_user = request.user
        if FriendRequest.objects.filter(requester=curr_user, receiver=requested_user):
            raise NotFound('You already sent this user a request')
        if requested_user == curr_user:
            raise NotFound('Yourself')
        FriendRequest.objects.create(status='pending', receiver=requested_user, requester=curr_user)
        return Response({'detail': 'Friend Request sent'})


# GET: List all open friend requests from others
class AllOpenRequestView(GenericAPIView):
    queryset = User.objects.all()

    def get(self, request, **kwargs):
        requests = FriendRequest.objects.filter(status='pending')
        serializer = PendingRequestSerializer(requests, many=True)
        return Response(serializer.data)


# GET: Logged in user’s pending friend requests
class PendingRequestView(GenericAPIView):

    def get(self, request, **kwargs):
        curr_user = request.user
        requests = FriendRequest.objects.filter(status='pending', receiver=curr_user)
        serializer = PendingRequestSerializer(requests, many=True)
        return Response(serializer.data)


# POST: Accept an friend request
class AcceptFriendRequestView(GenericAPIView):
    queryset = FriendRequest.objects.all()

    def post(self, request, pk, **kwargs):
        curr_req = self.get_object()
        accept_request = FriendRequest.objects.get(status='pending', id=curr_req)
        accept_request.status = 'accepted'
        accept_request.save()
        return Response({'detail': 'Accepted!'})


# POST: Reject a friend request
class RejectFriendRequestView(GenericAPIView):
    queryset = FriendRequest.objects.all()

    def post(self, request, pk, **kwargs):
        curr_req = self.get_object()
        reject_request = FriendRequest.objects.get(status='pending', id=curr_req)
        reject_request.status = 'reject'
        reject_request.save()
        return Response({'detail': 'Rejected!'})


# DELETE: A Friend
class DeleteFriendView(GenericAPIView):
    queryset = User.objects.all()

    def delete(self, request, pk, **kwargs):
        friend = self.get_object()
        delete_request = FriendRequest.objects.filter(Q(status='accepted'),
                                                      Q(requester=friend.id),
                                                      Q(receiver=pk) |
                                                      Q(status='accepted'),
                                                      Q(requester=pk),
                                                      Q(receiver=friend.id))
        if delete_request:
            delete_request.delete()
            return Response({'detail': 'Deleted!'})
        raise NotFound(f'you are not friend of this user!')
