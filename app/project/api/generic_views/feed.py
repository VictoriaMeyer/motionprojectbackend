from django.db.models import Q
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework import filters, generics
from project.api.models import Post, User
from project.api.serializers import PostSerializer, UserSerializer


# GET: Lists all the posts
class ListCreatePostsView(GenericAPIView):
    def get(self, request, **kwargs):
        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data)


# GET: Search posts of all users
class PostSearchView(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('id', 'title', 'content')


# GET: List created Posts by user
class PostUserView(GenericAPIView):
    queryset = User.objects.all()

    def get(self, request, pk, **kwargs):
        curr_user = self.get_object()
        serializer = PostSerializer(curr_user.posts.all(), many=True)
        return Response(serializer.data)


# GET: Lists all the posts of followed users
class FollowedUsersPostsView(GenericAPIView):
    def get(self, request, **kwargs):
        followees = request.user.followees.all()
        posts = Post.objects.filter(user__user_profile__in=followees)
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data)


# GET: List all the Friends
class AllFriendsView(GenericAPIView):

    def get(self, request, **kwargs):
        curr_user = request.user
        friends = User.objects.filter(Q(received__status='accepted') |
                                      Q(requested__status='accepted'),
                                      Q(received__requester=curr_user)).distinct()
        serializer = UserSerializer(friends, many=True)
        return Response(serializer.data)
