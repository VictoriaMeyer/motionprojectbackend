# Generated by Django 2.0.7 on 2018-10-25 06:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_post_shared'),
    ]

    operations = [
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.TextField(verbose_name='email')),
                ('validation_code', models.TextField(verbose_name='validation_code')),
            ],
        ),
        migrations.AddField(
            model_name='userprofile',
            name='validation_code',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='validation_code'),
        ),
        migrations.AlterField(
            model_name='friendrequest',
            name='receiver',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='received', to=settings.AUTH_USER_MODEL, verbose_name='user'),
        ),
    ]
