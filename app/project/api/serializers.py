from django.core.mail import EmailMessage
from rest_framework.exceptions import ValidationError
from project.api.models import Post, FriendRequest
from rest_framework import serializers
from django.contrib.auth import get_user_model
User = get_user_model()


#  All user Profile info
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email', 'followees']
        read_only_fields = ['id']


#  Public user Profile info
class PublicUserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name']
        read_only_fields = ['id']


#  All Post info
class PostSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)  # Create a sub serializer to represent the user

    class Meta:
        model = Post
        fields = ['id', 'created', 'user', 'title', 'content', 'shared']
        read_only_fields = ['id', 'created', 'user']

    def create(self, validated_data):
        return Post.objects.create(
            user=self.context.get('request').user,
            **validated_data,
        )


#  Post share serializer
class PostShareSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Post
        fields = ['id', 'created', 'user', 'title', 'content', 'shared']
        read_only_fields = ['id', 'created', 'user', 'title', 'content']

    def create(self, validated_data):
        return Post.objects.create(
            user=self.context.get('request').user,
            **validated_data,
        )


#  Pending Friend request
class PendingRequestSerializer(serializers.ModelSerializer):

    class Meta:
        model = FriendRequest
        fields = ['id', 'status', 'created', 'receiver', 'requester']
        read_only_fields = ['id']


class RegistrationSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate_email(self, email):
        try:
            User.objects.get(email=email)
            raise ValidationError('E-Mail address is already in use!')
        except User.DoesNotExist:
            return email

    def send_validation_email(self, email, validation_code):
        message = EmailMessage(
            to=[email],
            subject='Your Motion Registration',
            body=f'Your code is: {validation_code}',
        )
        message.send()

    def save(self, **kwargs):
        user = User.objects.create_user(
            username=kwargs.get('email'),
            email=kwargs.get('email'),
            is_active=False,
        )
        # user_profile = UserProfile.objects.create(
        #     user=user,
        # )
        user.user_profile.generate_validation_code()
        self.send_validation_email(user.email, user.user_profile.validation_code)
        return user


class RegistrationValidationSerializer(serializers.Serializer):
    code = serializers.CharField(required=True)
    username = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField()
    password_repeat = serializers.CharField()

    def validate_username(self, username):
        try:
            User.objects.get(username=username)
            raise ValidationError('Username is already taken!')
        except User.DoesNotExist:
            if '@' in username:
                raise ValidationError('Username cannot contain an @ sign!')
        return username

    def validate_email(self, email):
        try:
            return User.objects.get(email=email, is_active=False)
        except User.DoesNotExist:
            raise ValidationError('User does not exist or is active!')

    def validate(self, attrs):
        user = attrs.get('email')
        if user.user_profile.validation_code != attrs.get('code'):
            raise ValidationError({'code': 'Code not valid!'})
        if attrs.get('password') != attrs.get('password_repeat'):
            raise ValidationError({'password_repeat': 'Passwords do not match!'})
        return attrs

    def save(self, **kwargs):
        user = kwargs.get('email')
        user.username = kwargs.get('username')
        user.is_active = True
        user.set_password(kwargs.get('password'))
        user.save()
        user.user_profile.validation_code = ''
        user.user_profile.save()
        return user
