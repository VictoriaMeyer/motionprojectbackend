from django.urls import path
from project.api.generic_views.registration import RegistrationView, RegistrationValidationView
from project.api.generic_views.me import MeView
from project.api.generic_views.posts import GetUpdateDeletePostView, LikeDislikePostView, SharePostView, \
    CreateNewPostView
from project.api.generic_views.users import FollowView, FolloweesView, AllUsersView, UserSearchView, PublicUsersView, \
    AllOpenRequestView, FiendRequestView, PendingRequestView, AcceptFriendRequestView, RejectFriendRequestView, \
    DeleteFriendView, AllFollowersView
from project.api.generic_views.feed import ListCreatePostsView, PostUserView, PostSearchView, AllFriendsView, \
    FollowedUsersPostsView
from rest_framework_simplejwt.views import (
   TokenObtainPairView,
   TokenRefreshView,
   TokenVerifyView,
)

urlpatterns = [
    # Auth
    path('token/', TokenObtainPairView.as_view(), name='token-obtain-pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token-refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token-verify'),

    # Feed
    path('feed/', ListCreatePostsView.as_view(), name='list-create-posts'),
    path('feed/<int:pk>/', PostUserView.as_view(), name='list-create-posts-by-user-id'),
    path('feed/followers/', FollowedUsersPostsView.as_view(), name='lists-posts-followed-users'),
    path('feed/search/', PostSearchView.as_view(), name='post-search'),
    path('feed/friends/', AllFriendsView.as_view(), name='get-all-friends'),

    # Posts
    path('posts/<int:pk>/', GetUpdateDeletePostView.as_view(), name='posts-by-id'),
    path('posts/new-post/', CreateNewPostView.as_view(), name='new-post-by-user-id'),
    path('posts/likes/', LikeDislikePostView.as_view(), name='list-posts-user-like'),
    path('posts/like/<int:pk>/', LikeDislikePostView.as_view(), name='like-dislike-post'),
    path('posts/share-post/<int:pk>/', SharePostView.as_view(), name='user-share-post'),

    # Users
    path('users/followers/', AllFollowersView.as_view(), name='list-all-followers'),
    path('users/following/', FolloweesView.as_view(), name='list-all-followees'),
    path('users/follow/<int:pk>/', FollowView.as_view(), name='follow-unfollow-a-user'),
    path('users/', AllUsersView.as_view(), name='get-users'),
    path('users/search/', UserSearchView.as_view(), name='search-users'),
    path('users/<int:pk>/', PublicUsersView.as_view(), name='specific-user-profile'),
    path('users/friendrequests/', AllOpenRequestView.as_view(), name='all-opend-friendrequest'),
    path('users/friendrequests/<int:pk>/', FiendRequestView.as_view(), name='send-friendrequest'),
    path('users/friendrequests/pending/', PendingRequestView.as_view(), name='pending-friendrequest-current-user'),
    path('users/friendrequests/accept/<int:pk>/', AcceptFriendRequestView.as_view(), name='accept-friendrequest'),
    path('users/friendrequests/reject/<int:pk>/', RejectFriendRequestView.as_view(), name='reject-friendrequest'),
    path('users/friends/', AllFriendsView.as_view(), name='get-all-friends'),
    path('users/friends/unfriend/<int:pk>/', DeleteFriendView.as_view(), name='get-all-friends'),

    # Me
    path('me/', MeView.as_view(), name='get-update-userprofile'),

    # Registration Validation
    path('registration/', RegistrationView.as_view(), name='registration'),
    path('registration/validation/', RegistrationValidationView.as_view(), name='registration-validation'),
]
