from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from project.api.serializers import RegistrationSerializer, RegistrationValidationSerializer


class RegistrationView(GenericAPIView):
    serializer_class = RegistrationSerializer
    permission_classes = [AllowAny]

    def post(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.save(**serializer.validated_data)
            return Response({'detail': f'Registration validation email sent to {user.email}!'})


class RegistrationValidationView(GenericAPIView):
    serializer_class = RegistrationValidationSerializer
    permission_classes = [AllowAny]

    def post(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.save(**serializer.validated_data)
            return Response({'detail': f'Registration successful for {user.email}!'})
