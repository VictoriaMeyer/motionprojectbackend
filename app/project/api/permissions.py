from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.user == request.user


class CurrentUserIsReceiver(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if obj.receiver == request.user:
            return True
        return False


class CUrrentUserIsRequester(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if obj.requester == request.user:
            return True
        return False
